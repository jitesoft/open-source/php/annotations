<?php
namespace Jitesoft\Annotations\Tests;

use Doctrine\Common\Annotations\AnnotationException;
use Jitesoft\Annotations\Scopes;
use ReflectionClass;

class MethodScopeTest extends TestCase {

    public function testMethodAnnotationSuccess() {
        $class = new ReflectionClass(Test_MethodScope::class);
        $method = $class->getMethod('valid');
        $annotation = $this->reader->getMethodAnnotation($method, Scopes::class);

        $expects = (new Test_MethodScope())->valid();
        $this->assertEquals($expects, $annotation->scopes);

    }

    public function testMethodAnnotationFailure() {
        $class = new ReflectionClass(Test_MethodScope::class);
        $method = $class->getMethod('invalid');
        $this->expectException(AnnotationException::class);
        $this->reader->getMethodAnnotation($method, Scopes::class);
    }

    public function testClassAnnotationSuccess() {
        $class = new ReflectionClass(Test_ClassScopeValid::class);
        $annotation = $this->reader->getClassAnnotation($class, Scopes::class);
        $this->assertEquals(Test_ClassScopeValid::VALUES, $annotation->scopes);
    }

    public function testClassAnnotationFailure() {
        $class = new ReflectionClass(Test_ClassScopeInvalid::class);
        $this->expectException(AnnotationException::class);
        $this->reader->getClassAnnotation($class, Scopes::class);
    }

}

/**
 * @Scopes(123)
 */
class Test_ClassScopeInvalid { }

/**
 * @Scopes({"a", "b", "c"})
 */
class Test_ClassScopeValid {
    public const VALUES = ['a', 'b', 'c'];
}

class Test_MethodScope {
    /**
     * @Scopes({"a", "b", "c"})
     */
    public function valid() { return ['a', 'b', 'c']; }
    /**
     * @Scopes(1)
     */
    public function invalid() { }
}
