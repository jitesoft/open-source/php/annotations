<?php
namespace Jitesoft\Annotations\Tests;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use PHPUnit\Framework\TestCase as TC;

abstract class TestCase extends TC {

    /**
     * @var AnnotationReader
     */
    protected $reader;

    protected function setUp() {
        parent::setUp();

        AnnotationRegistry::registerLoader('class_exists');
        $this->reader = new AnnotationReader();
    }

}
