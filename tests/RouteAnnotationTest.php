<?php
namespace Jitesoft\Annotations\Tests;

use Doctrine\Common\Annotations\AnnotationException;
use Jitesoft\Annotations\Route;
use ReflectionClass;

class RouteAnnotationTest extends TestCase {

    public function testValid() {
        $class = new ReflectionClass(Test_RouteValid::class);
        $method = $class->getMethod('getSomething');
        $annotation = $this->reader->getMethodAnnotation($method, Route::class);

        $this->assertEquals('GET', $annotation->getMethod());
        $this->assertEquals('/a/route', $annotation->getRoute());
    }

    public function testInvalid() {
        $class = new ReflectionClass(Test_RouteInvalid::class);
        $method = $class->getMethod('getSomething');
        $this->expectException(AnnotationException::class);
        $this->reader->getMethodAnnotations($method);
    }

    public function testInvalidWithRoute() {
        $class = new ReflectionClass(Test_RouteInvalid::class);
        $method = $class->getMethod('postSomething');
        $this->expectException(AnnotationException::class);
        $this->reader->getMethodAnnotations($method);
    }

    public function testInvalidWithMethod() {
        $class = new ReflectionClass(Test_RouteInvalid::class);
        $method = $class->getMethod('getSomethingElse');
        $this->expectException(AnnotationException::class);
        $this->reader->getMethodAnnotations($method);
    }
}

class Test_RouteInvalid {
    /**
     * @Route(method="NONE", route="/a/route")
     */
    public function postSomething() {
    }

    /**
     * @Route(method="GET")
     */
    public function getSomething() {}


    /**
     * @Route(route="/a/b/c")
     */
    public function getSomethingElse() {}
}

class Test_RouteValid {
    /**
     * @Route(method="GET", route="/a/route")
     */
    public function getSomething() { }
}
