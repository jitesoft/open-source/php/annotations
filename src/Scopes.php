<?php
namespace Jitesoft\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * Annotation used to define custom scopes on Controller classes and methods.
 *
 * @Annotation
 * @Target({"METHOD", "CLASS"})
 */
class Scopes {

    /**
     * @var array<string>
     */
    public $scopes;

    /**
     * @return array|string[]
     */
    public function getScopes(): array {
        return $this->scopes;
    }

}
