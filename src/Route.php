<?php
namespace Jitesoft\Annotations;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Enum;
use Doctrine\Common\Annotations\Annotation\Required;
use Doctrine\Common\Annotations\AnnotationException;

/**
 * Annotation used to define a route on actions inside a controller class.
 *
 * @Annotation
 * @Annotation\Target("METHOD")
 */
class Route {
    private const METHODS = [ 'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH' ];

    /**
     * @var string
     * @Required
     */
    public $route;

    /**
     * @Required
     * @Enum({"GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"})
     */
    public $method;

    /**
     * @return string
     */
    public function getRoute(): string {
        return $this->route;
    }

    /**
     * @return string
     */
    public function getMethod(): string {
        return $this->method;
    }

    /**
     * Route constructor.
     * @param array $values
     * @throws AnnotationException
     */
    public function __construct(array $values) {
        if (!array_key_exists('method', $values)) {
            throw new AnnotationException('Missing required parameter "method".');
        }

        if (!array_key_exists('route', $values)) {
            throw new AnnotationException('Missing required parameter "route".');
        }

        if (!in_array($values['method'], self::METHODS)) {
            throw new AnnotationException('method must match one of following values: ' . implode(',', self::METHODS));
        }

        $this->method = $values['method'];
        $this->route  = $values['route'];
    }

}
