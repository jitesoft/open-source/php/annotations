# Annotations

This project contains custom annotations used in various projects.

Feel free to use, add, modify or whatever, PR's accepted!


## Implemented annotations:

### Scopes

Scopes are intended to be used on custom scope annotations in controllers.  
The Scopes annotation takes a array of strings which can then be fetched to check with a specific users scope.

```php
<?php

use Jitesoft\Annotations\Scopes;

/**
 * @Scopes({"read:resource"})
 */
class MyController {    
    /**
    * @Scopes({"delete:resource"})
    */
    public function deleteSomething() {}
}

// Read the annotation class with the doctrine annotation reader:
AnnotationRegistry::registerLoader('class_exists');

$this->reader = new AnnotationReader();
$class        = new ReflectionClass(Test_MethodScope::class);
$method       = $class->getMethod('deleteSomething');
$methodScopes = $this->reader->getMethodAnnotation($method, Scopes::class);
$classScopes  = $this->reader->getClassAnnotation($class, Scopes::class);


var_dump($methodScopes->getScopes()); /** [ 'delete:resource' ] */
var_dump($classScopes->getScopes());  /** [ 'read:resource' ] */
```
